const express = require('express');
const { LoadService } = require('../services/loadService');
const { asyncWrapper } = require('../util/asyncWrapper');
const { ifShipper } = require('./middlewares/checkIfShipperMiddleware');
const { ifDriver } = require('./middlewares/checkIfDriverMiddle')

const router = express.Router();

const loadService = LoadService.getInstance();

router.post('/', asyncWrapper(ifShipper), asyncWrapper(async function(req, res) {
    const id = req.user._id;
    const { name, payload, pickup_address, delivery_address, dimensions } = req.body;
    const load = await loadService.add(name, payload, pickup_address, delivery_address, dimensions, id);
    return res.status(200).json({message: 'Load created successfully'});
}));

router.get('/', asyncWrapper(async function(req, res) {
    const {skip = 0, limit = 10} = req.query;
    const id = req.user._id;

    const loads = await loadService.get(skip, limit, id);
    
    return res.status(200).json({loads: loads});
}));

router.post('/:id/post', asyncWrapper(ifShipper), asyncWrapper(async function(req, res) {
    const id = req.user._id;

    const answer = await loadService.findDriver(id);

    if(answer) {
        return res.status(200).json({message: "Load posted successfully",
                                    driver_found: answer});
    }
    if(!answer) {
        return res.status(200).json({message: "Please try again",
                                    driver_found: answer});
    }  
}));

router.get('/active', asyncWrapper(ifDriver), asyncWrapper(async function(req, res) {
    const id = req.user._id;

    const activeLoad = await loadService.getActive(id);
    return res.status(200).json({load: activeLoad});
    
}));

router.patch('/active/state', asyncWrapper(ifDriver), asyncWrapper(async function(req, res) {
    const id = req.user._id;

    const state = await loadService.changeState(id);

    if(state){
        return res.status(200).json({message: `Load state changed to ${state.state}`});
    }
    if(!state){
        return res.status(200).json({message: `The load has been already shipped`});
    }
}));

router.get('/:id', asyncWrapper(async function(req, res) {
    const loadId = req.params.id;
    const id = req.user._id;

    const load = await loadService.getById(loadId, id);
    
    return res.status(200).json({load: load});
}));

router.put('/:id', asyncWrapper(ifShipper), asyncWrapper(async function(req, res) {
    const loadId = req.params.id;
    const id = req.user._id;
    const { name, payload, pickup_address, delivery_address, dimensions } = req.body;
    const load = await loadService.update(name, payload, pickup_address, delivery_address, dimensions, id, loadId);
    return res.status(200).json({message: 'Load details changed successfully'});
}));

router.delete('/:id', asyncWrapper(ifShipper), asyncWrapper(async function(req, res) {
    const loadId = req.params.id;
    const id = req.user._id;

    const load = await loadService.delete( loadId, id);
    
    return res.status(200).json({message: 'Load deleted successfully'});
}))

router.get('/:id/shipping_info', asyncWrapper(ifShipper), asyncWrapper(async function(req, res) {
    const id = req.user._id;
    const loadId = req.params.id;

    const load = await loadService.getInfo(id, loadId);
    
    if(load){
        return res.status(200).json({load: load});
    }
    if(!load){
        return res.status(200).json({message: 'No active load!'});
    }
}));




module.exports = router;