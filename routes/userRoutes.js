const express = require('express');
const { UserService } = require('../services/userService');
const { UserDTO } = require('../dto/user.dto');
const { asyncWrapper } = require('../util/asyncWrapper');
const passwordValid = require('../joiValidationSchema/passwordValidationSchema');
const multer  = require('multer');
const storage = multer.diskStorage({
    destination: (req, file, cb)=>{
        cb(null, '../uploads')
    },
    filename: (req, file, cb)=>{
        cb(null, file.originalname)
    },
})
const uploadPhoto = multer({dest: '../uploads', storage})

const router = express.Router();

const userService = UserService.getInstance();


router.get('/me', asyncWrapper(async function(req, res) {
    const user = await userService.get(req.user._id);
    return res.status(200).json({user: new UserDTO(user)});
}));

router.delete('/me', asyncWrapper(async function(req, res) {
    const user = await userService.delete(req.user._id);
    return res.status(200).json({message: 'Profile deleted successfully'});
}));

router.patch('/me/password', passwordValid, asyncWrapper(async function(req, res) {
  const {oldPassword, newPassword} = req.body;
  const user = await userService.get(req.user._id, oldPassword, newPassword);
    return res.status(200).json({message: 'Password changed successfully'});
}));

router.post('/me/upload', uploadPhoto.single('avatar'), asyncWrapper(async function(req, res) {
    const id = req.user._id;
    const user = await userService.uploadPhoto(id);

    return res.status(200).json({message: 'Success'});
}));


module.exports = router;