const express = require('express');
const { User } = require('../../models/User');


const ifDriver = async function(req, res, next) {
      const user = await User.findOne({_id: req.user._id});

      if (user.role !== 'DRIVER') {
        throw new Error('Not a driver!')
      }
  
    next();
  };

  module.exports = { ifDriver };