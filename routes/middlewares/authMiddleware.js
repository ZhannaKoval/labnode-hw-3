const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../../config');

const authMiddleware = (req, res, next) => {
  const header = req.headers['authorization'];

  if (!header) {
    return res.status(401).json({message: `No Authorization header found!`});
  }

  const [tokenType, token] = header.split(' ');

  if (!token) {
    return res.status(401).json({message: `No JWT token found!`});
  }

  req.user = jwt.verify(token, JWT_SECRET);
  next();
};

module.exports = { authMiddleware };