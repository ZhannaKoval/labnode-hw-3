const express = require('express');
const { User } = require('../../models/User');


const ifShipper = async function(req, res, next) {
      const user = await User.findOne({_id: req.user._id});

      if (user.role !== 'SHIPPER') {
        throw new Error('Not a shipper!')
      }
  
    next();
  };

  module.exports = { ifShipper };