const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  photo: {
    type: Boolean,
    default: false
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
    unique: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
  role: {
    type: String,
    enum: ['SHIPPER', 'DRIVER'],
    default: 'SHIPPER'
  },
  trucks: [{
    ref: 'trucks',
    type: Schema.Types.ObjectId,
  }],
  loads: [{
    ref: 'loads',
    type: Schema.Types.ObjectId, 
  }]
});

module.exports.User = mongoose.model('users', userSchema);