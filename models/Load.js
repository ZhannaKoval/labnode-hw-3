const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const loadSchema = new Schema({
  created_by: {
    ref: 'users',
    type: Schema.Types.ObjectId,
  },
  assigned_to: {
    ref: 'users',
    type: Schema.Types.ObjectId,
    default: null
  },
  status: {
    type: String,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    default: 'NEW'
  },
  state: {
    type: String,
    enum: ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery'],
    default: 'En route to Pick Up'
  },
  name:{
    type: String,
    required: true
  },
  payload: {
    type: Number,
    required: true
  }, 
  pickup_address: {
    type: String,
    required: true
  },
  delivery_address: {
    type: String,
    required: true
  },
  dimensions: {
    width: {
        type: Number,
        required: true
    },
    length: {
        type: Number,
        required: true 
    },
    height: {
        type: Number,
        required: true 
    }
  },
  logs: [{
    message: {
        type: String,
        default: 'En route to Pick Up'
    },
    time: {
        type: Date,
        default: Date.now()
    }
  }],
  createdDate: {
    type: Date,
    default: Date.now()
  },
});

module.exports.Load = mongoose.model('loads', loadSchema);