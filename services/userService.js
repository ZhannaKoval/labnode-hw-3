const { User } = require('../models/User');
const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');

class UserService {

   async create(email, password, role) {
        const hashedPass = await bcrypt.hash(password, 10);
    
        const user = new User({
            email,
            password: hashedPass,
            role
          });
        
        await user.save();
        
        return user;
    }

    async login(email, password) {
        const user = await User.findOne({email});

        if (!user) {
            throw new Error (`Wrong email!`);
        }

        const compareRes = await bcrypt.compare(password, user.password);


        if (!compareRes) {
            throw new Error (`Wrong password!`);
        }
        
        return user;
    }

    get(id) {
        return User.findOne({ _id: id });
    }

    delete(id) {
        return User.deleteOne({ _id: id });
    }

    async update(id, { oldPassword, newPassword }) {
        
            const user = await User.findOne({_id: id});

            const passwordsMatch = await bcrypt.compare(newPassword, user.password);

            if (passwordsMatch) {
                throw new Error (`Wrong old password!`);
            }
        
            if(oldPassword.split("").sort().join("") === newPassword.split("").sort().join("")) {
                throw new Error (`Passwords are the same!`);
            }
        
            user.password = await bcrypt.hash(newPassword, 10);;
            return user.save();
    }

    async getNewPass(email) {
        const user = await User.findOne({email: email});
        const newPass = '123456';
        
        const transporter = nodemailer.createTransport({
            port: 8080,              
            host: "localhost",
               auth: {
                    user: "test_email@gmail.com",
                    password: "testpass"
                 },
            secure: false,
            });

        const mailData = {
            from: "no-reply@gmail.com",
            to: email,
            subject: "Forgot your password?",
            text:  `Your new password is ${newPass}` 
        }

       const result = await transporter.sendMail(mailData)

       user.password = newPass;
       await user.save();

        return result;

    }

    async uploadPhoto(id){
        const user = await User.findOne({ _id: id });
        user.photo = true;
        await user.save();
        return true;
    }
}

UserService.getInstance = () => {
    if (!UserService.instance) {
        UserService.instance = new UserService();
    }

    return UserService.instance;
};

module.exports = { UserService };