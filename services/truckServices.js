
const { Truck } = require('../models/Truck');

class TruckService {

    async add (id, type) {
        const truck = new Truck({
            created_by: id,
            type: type
          });

        
        await truck.save();
        
        return truck;
    }

    async get(id) {
        
        return Truck.find({created_by: id });

    }

    async getById(truckId, id){
        
        const certainTruck = await Truck.findOne({_id: truckId, created_by: id});

        if (!certainTruck) {
            throw new Error('Wrong id');
        }
        return certainTruck;
    }

    async update(type, truckId, id) {
        const certainTruck = await Truck.findOne({_id: truckId, created_by: id});
        
        if (!certainTruck) {
            throw new Error ('Wrong id');
        }

        if(JSON.stringify(certainTruck.assigned_to) === JSON.stringify(certainTruck.created_by)) {
            throw new Error ('The truck is assigned to you!');
        }

        certainTruck.type = type;
        await certainTruck.save();

        return certainTruck;
    }

    async delete(truckId, id){
        const certainTruck = await Truck.findOne({_id: truckId, created_by: id});
        if (!certainTruck) {
            throw new Error ('Wrong id');
        }

        if(JSON.stringify(certainTruck.assigned_to) === JSON.stringify(certainTruck.created_by)) {
            throw new Error ('The truck is assigned to you!');
        }

        certainTruck.deleteOne({_id: truckId});
        return true;
    }

    async assign(truckId, id) {
        const certainTruck = await Truck.findOne({_id: truckId, created_by: id, assined_to: {$exists: 0}});
        if (!certainTruck) {
            throw new Error ('Wrong id');
        }

        /*if(certainTruck.assigned_to === id) {
            throw new Error ('The truck is already assigned to you!');
        }*/

        certainTruck.assigned_to = id;
        await certainTruck.save();

        return certainTruck;
    }


    
}

TruckService.getInstance = () => {
    if (!TruckService.instance) {
        TruckService.instance = new TruckService();
    }

    return TruckService.instance;
};

module.exports = { TruckService };
