const { Load } = require('../models/Load');
const { Truck } = require('../models/Truck');
const { User } = require('../models/User');

class LoadService {
    async add(name, payload, pick_add, del_add, dimensions, id){
        const load = new Load({
            name: name,
            payload: payload,
            pickup_address: pick_add,
            delivery_address: del_add,
            dimensions: {
                width: dimensions.width,
                length: dimensions.length,
                height: dimensions.height
            },
            created_by: id
          });

        
        await load.save();
        
        return load;
    }

    async get(skip, limit, id){
        const user = await User.findOne({_id: id});
        let allLoads;
        if(user.role === 'DRIVER'){
            console.log('yes')
            const truck = await Truck.findOne({assigned_to: user._id});
             allLoads = await Load.find(
                {assigned_to: truck._id},
                [],
                {
                  skip: parseInt(skip),
                  limit: limit > 100 ? 3 : parseInt(limit),
                },
            );
        }
        
        if(user.role === 'SHIPPER'){
            allLoads = await Load.find(
            {created_by: id},
            [],
            {
              skip: parseInt(skip),
              limit: limit > 100 ? 3 : parseInt(limit),
            },
        );
        }

        return allLoads;
    }

    async getActive(id){
        const truck = await Truck.findOne({assigned_to: id})
        console.log(truck)
        return Load.findOne({assigned_to: truck._id, status: {$ne: 'SHIPPED'}});
    }

    async findDriver(loadId){ 
        const load = await Load.findOne({created_by: loadId});
        load.status = 'POSTED';
        load.logs.push ({
            message: `Load posted successfully`,
            time: Date.now()
        });
        await load.save();
        let truck;

        if(load.payload <= 1700 && load.dimensions.width <= 300){
             truck = await Truck.findOne({status: 'IS', assigned_to: {$exists:true}, type: 'SPRINTER'});
        } 
        if (load.payload <= 2500 && load.dimensions.width > 300 && load.dimensions.width <= 500){
            truck = await Truck.findOne({status: 'IS', assigned_to: {$exists:true}, type: 'SMALL STRAIGHT'})
        } 
        if(load.payload >= 4000 && load.dimensions.width >= 700) {
            truck = await Truck.findOne({status: 'IS', assigned_to: {$exists:true}, type: 'LARGE STRAIGHT'})
        }

        if(truck) {
            load.status = 'ASSIGNED';
            truck.status = 'OL';
            load.assigned_to = truck._id;
            load.logs.push ({
                message: `Load assigned to driver with id ${truck.created_by}`,
                time: Date.now()
            });
            await load.save();
            await truck.save();
            return true;
        } else {
            load.status = 'NEW';
            load.logs.push ({
                message: `The driver was not found`,
                time: Date.now()
            });
            await load.save();
            return false;
        }
    }

    async changeState(id){
        const truck = await Truck.findOne({assigned_to: id})
        const activeLoad = await Load.findOne({assigned_to: truck._id, status: {$ne: 'SHIPPED'}});
        if(activeLoad){
            if(activeLoad.state === 'En route to Pick Up'){
                activeLoad.state = 'Arrived to Pick Up';
                activeLoad.logs.push ({
                    message: `Arrived to Pick Up`,
                    time: Date.now()
                });
                await activeLoad.save();
            }
            if (activeLoad.state === 'Arrived to Pick Up'){
                activeLoad.state = 'En route to delivery';
                activeLoad.logs.push ({
                    message: `En route to delivery`,
                    time: Date.now()
                });
                await activeLoad.save();
            } else if(activeLoad.state === 'En route to delivery'){
                activeLoad.state = 'Arrived to delivery';
                activeLoad.logs.push ({
                    message: `Arrived to delivery`,
                    time: Date.now()
                });
                activeLoad.status = 'SHIPPED';
                await activeLoad.save();
            } 
        }

        return activeLoad;
    }

    async getById(loadId, id) {
        const certainLoad = await Load.findOne({_id: loadId, created_by: id});

        if (!certainLoad) {
            throw new Error('Wrong id');
        }
        return certainLoad;
    }

    async update(name, payload, pick_add, del_add, dimensions, id, loadId){
        const load = await Load.findOne({_id: loadId, created_by: id});
        if (!load) {
            throw new Error('Wrong id');
        }
        load.name = name;
        load.payload = payload;
        load.pickup_address = pick_add;
        load.delivery_address= del_add;
        load.dimensions.width = dimensions.width;
        load.dimensions.length = dimensions.length;
        load.dimensions.height = dimensions.height;
        load.logs.push ({
            message: `Information on load was updated`,
            time: Date.now()
        });
        await load.save();

        return load;
    }

    async delete(loadId, id){
        const load = await Load.findOne({_id: loadId, created_by: id});
        console.log(load)
        if (!load) {
            throw new Error ('Wrong id');
        }

        if(load.assigned_to){
            throw new Error ('The load is already assigned')
        }

        load.deleteOne({_id: loadId});
        return true;
    }

    getInfo(id, loadId){
        return  Load.findOne({_id: loadId, created_by: id, status: {$ne: 'SHIPPED'}});
    }
}

LoadService.getInstance = () => {
    if (!LoadService.instance) {
        LoadService.instance = new LoadService();
    }

    return LoadService.instance;
};

module.exports = { LoadService };