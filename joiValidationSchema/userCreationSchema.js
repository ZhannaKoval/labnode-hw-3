const Joi = require('joi');

module.exports = async function(req, res, next) {
const schema = Joi.object({
    email: Joi.string()
        .min(1)
        .max(30)
        .required(),

    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{2,30}$')),
        
    role: Joi.string()
  });

  await schema.validateAsync(req.body)
      .then(() => {
        next();
      })
      .catch((err) => {
        return res.status(400).json({message: err.message});
      });
}

