const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const { authMiddleware } = require('./routes/middlewares/authMiddleware');
const { errorHandler } = require('./routes/middlewares/errorHandler');
const userRoutes = require ('./routes/userRoutes');
const truckRoutes = require('./routes/truckRoutes');
const loadRoutes = require('./routes/loadRoutes');
const authRoutes = require ('./routes/authRoutes');
const { ifDriver } = require('./routes/middlewares/checkIfDriverMiddle') 
const { asyncWrapper } = require('./util/asyncWrapper');
 

const PORT = process.env.PORT || 8080;

app.use(morgan('dev'));
app.use(cors('dev'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use('/api/auth', authRoutes);
app.use('/api/users', authMiddleware, userRoutes); 
app.use('/api/trucks', authMiddleware, asyncWrapper(ifDriver), truckRoutes);
app.use('/api/loads', authMiddleware, loadRoutes)

app.use(errorHandler);


app.use((err, req, res, next) => {
  return res.status(400).json({message: err.message});
});

const start = async () => {
  try {
    await mongoose.connect('mongodb+srv://Zhanna_Koval:1408@cluster0.hmzc9.mongodb.net/DBforUber?retryWrites=true&w=majority', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });
    app.listen(PORT, ()=>{
      console.log(`Server has been started on port ${PORT}`);
    });
  } catch (e) {
    console.log(e);
  }
}

start();

app.all('*', function(req, res, next) {
  res.status(404).send('Not found');
});
