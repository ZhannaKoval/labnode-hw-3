class UserDTO {
    constructor({ _id, email, createdDate }) {
        this._id = _id;
        this.email = email;
        this.createdDate = createdDate;
    }
}

module.exports = { UserDTO };
